<!DOCTYPE html>
<html lang="ru">
<head>
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Form PHP</title>
	<style>
        .error {
            border:3px solid red;
            font-size:14pt;
            padding: 2px;
            margin: 0px auto;
            width: 400px;
        }
        .complete {
            margin: 0px auto;
            width: 400px;
            border:5px solid greenyellow;
            font-size:14pt;
        }
        body {
            max-width: 960px;
            margin: 0 auto;
            font-family: Arial, Helvetica, sans-serif;
            background-color: darkcyan;
        }
        form {
            border-style: solid;
            border-color: black;
            border-size:1px;
            font-size:16pt;
            padding:5px;
            width: 400px;
            margin: 0 auto;
            text-align:center;
        }
        .complete2 {
            margin: 0px auto;
            width: 400px;
            border:5px solid green;
            font-size:14pt;
        }
        .error1 {
            border:5px solid red;
            font-size:14pt;
            padding: 2px;
            margin: 0px auto;
            width: 400px;
            }
        .complete3  {
            width: 400px;
            border:5px solid darkblue;
            font-size:14pt;
            padding: 2px;
            margin: 0px auto;
        }
	</style>
</head>
<body>
<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

//первое поле - для скрытой передачи CSRF токена, чтобы нельзя было сделать POST запрос со стороны
?>
<form action="" method="POST">
    <input name="csrf_token" type="hidden" value="<?php print ($_SESSION['uid']); ?>" />
    <label>
      Имя: <br />
      <input name="name" <?php if ($errors['name']) {print 'class="error"';} ?> value="<?php print $values['name']; ?>" type="text"/>
    </label><br />
    <label>
      Email:<br />
      <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"
        value="sample@example.com"
        type="text" />
    </label><br />
    <label>
      Год рождения:<br />
      <select name="year" <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year']; ?>">
      <option value="выбрать...">выбрать...</option>
   <?php for($i = 1900; $i < 2022; $i++) {?>
  	<option <?php if ($values['year']==$i){print 'selected="selected"';} ?> value="<?php print $i; ?>"><?= $i; ?></option>
  	<?php }?>
    </select>
    </label><br />
    Пол:<br />
    <label><input type="radio"
      name="sex" <?php if ($values['sex']==0){print 'checked';} ?> value="0" />
      М</label>
    <label><input type="radio" <?php if ($values['sex']==1){print 'checked';} ?>
      name="sex" value="1" />
      Ж</label><br />
    Количество конечностей:<br />
    <label><input type="radio" <?php if ($values['limb']==0 || $values['limb']==1){print 'checked';} ?>
      name="limb" value="1" />
      1</label>
    <label><input type="radio" <?php if ($values['limb']==2){print 'checked';} ?>
      name="limb" value="2" />
      2</label>
      <label><input type="radio" <?php if ($values['limb']==3){print 'checked';} ?>
      name="limb" value="3" />
      3</label>
      <label><input type="radio" <?php if ($values['limb']==4){print 'checked';} ?>
      name="limb" value="4" />
      4</label>   
	 <label><br/>
        Способности:
        <br />
        <select name="power[]" <?php if ($errors['power']) {print 'class="error"';} ?>
          multiple="multiple">
          <option <?php if (in_array("god",$values['power'])){print 'selected="selected"';} ?> value="god">бессмертие</option>
          <option <?php if (in_array("clip",$values['power'])){print 'selected="selected"';} ?> value="clip">прохождение сквозь стены</option>
          <option <?php if (in_array("fly",$values['power'])){print 'selected="selected"';} ?> value="fly">левитация</option>
        </select>
    </label><br />
    <label>
      Биография (необязательно):<br />
      <textarea class="text" name="bio" placeholder="Your biography" rows=10><?php print $values['bio']; ?></textarea>
    </label><br />
    <label><input type="checkbox"
      name="check" required />
      С контрактом ознакомлен</label><br />
    <input type="submit" class="submit" value="Отправить" />
 </form> 
</body>
</html>