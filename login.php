<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
// Начинаем сессию.
session_start();
//запрещаем доступ к логину, если сидим под админом
function checkadmin() {
    if (!empty($_SERVER['PHP_AUTH_USER']) && !empty($_SERVER['PHP_AUTH_PW'])) {
    $user = 'u35653';
    $pass = '4017880';
    $db = new PDO('mysql:host=localhost;dbname=u35653', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    try {
        $stmt = $db->prepare("SELECT pwd FROM adminpassword WHERE login=:i");
        $result = $stmt->execute(array("i"=> $_SERVER['PHP_AUTH_USER']));
        $hashpwd = (current(current($stmt->fetchAll(PDO::FETCH_ASSOC))));
    }
    catch(PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }
    if (password_verify($_SERVER['PHP_AUTH_PW'], $hashpwd)) {
        return true;
    }
    else return false;
    }
    else return false;
}
// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.

if( isset( $_POST['sessiondestroy'] ) )
    {
      session_destroy();
      header('Location: ./');
      exit();
    }
if (!empty($_SERVER['PHP_AUTH_USER']) &&
    !empty($_SERVER['PHP_AUTH_PW']) && checkadmin()) {
    printf('<h3>Вы вошли на сайт под учетной записью администратора. <a href="admin.php">Выйдите</a> из нее для доступа к обычной форме авторизации</h3>');
    exit();
}
if (!empty($_SESSION['login'])) {
    ?>
    <form method="POST">
        <input type="submit" name="sessiondestroy" value="Выход" />
    </form>
    <?php
}
// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>

<form style="text-align: center" action="" method="post">
  <input name="login" />
  <input name="pwd" />
  <input type="submit" value="Войти" />
</form>

<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
  $user = 'u35653';
  $pass = '4017880';
  $db = new PDO('mysql:host=localhost;dbname=u35653', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  try {
    $stmt = $db->prepare("SELECT pwd FROM userpassword WHERE login=:i");
    $result = $stmt->execute(array("i"=>$_POST['login']));
    $hashpwd = (current(current($stmt->fetchAll(PDO::FETCH_ASSOC))));
  }
  catch(PDOException $e) {
      print('Error : ' . $e->getMessage());
      exit();
  }
  if (empty($hashpwd)){
    print ("Нет пользователя");
  }
  else {
    if (password_verify($_POST['pwd'], $hashpwd)) {
      $_SESSION['uid'] = substr( str_shuffle( 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM' ), 0, 10 );
      $_SESSION['login'] = $_POST['login'];
      header('Location: ./');
      exit();
  // Делаем перенаправление.
  } else {
      print( 'Пароль неправильный.');
  }
  }

  exit();
}
?>