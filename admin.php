<!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Form PHP</title>
    <style>
        body {
            margin: 0 auto;
            font-family: Arial, Helvetica, sans-serif;
            background-color: darkcyan;
            text-align: center;
        }
        table {
            margin: auto;
            background-color: white;
        }
        table th {
            font-weight: bold;
            padding: 5px;
        }
        .tr1 {
            margin: auto;
            margin-bottom: 20px;
        }
        .h0 {
            font:24pt sans-serif;
            text-align:left;
            width: 100%;
            margin: 0px;
        }
        .exit {
            float: right;
            box-sizing: border-box;
        }
        .exitbutton {
            width: 100px;
            height: 30px;
            box-sizing: border-box;
        }
        .selectlist {
            font: 16pt sans-serif;
        }
        .editbtn {
            width: 100px;
            height: 30px;
        }
    </style>
</head>
<body>
<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/


if ($_SERVER['REQUEST_METHOD'] == 'GET') {
$pass = false;
//проверка логина и пароль (если не пустой)
if (!empty($_SERVER['PHP_AUTH_USER']) ||
    !empty($_SERVER['PHP_AUTH_PW'])) {
    $user = 'u35653';
    $pass = '4017880';
    $db = new PDO('mysql:host=localhost;dbname=u35653', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    try {
        $stmt = $db->prepare("SELECT pwd FROM adminpassword WHERE login=:i");
        $result = $stmt->execute(array("i"=> $_SERVER['PHP_AUTH_USER']));
        $hashpwd = (current(current($stmt->fetchAll(PDO::FETCH_ASSOC))));
    }
    catch(PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }
    if (password_verify($_SERVER['PHP_AUTH_PW'], $hashpwd)) {
        $pass = true;
        //очищаем данные прошлой обычной сессии
        session_start();
        $_SESSION['uid']=0;
        $_SESSION['login']=0;
        session_destroy();
    }
}


if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW']) || $pass==false
    // || $_SERVER['PHP_AUTH_USER'] != 'admin' ||
    //md5($_SERVER['PHP_AUTH_PW']) != md5('123')
    ) {
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="My site"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}
//если авторизация успешная
print('<div class="h0"> Панель администратора');

$user = 'u35653';
$pass = '4017880';
$db = new PDO('mysql:host=localhost;dbname=u35653', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
try {
    $stmt = $db->prepare("SELECT id,login FROM userpassword");
    $result = $stmt->execute(array("i"=> $_SESSION['login']));
    $idbd = ($stmt->fetchAll(PDO::FETCH_ASSOC));
}
catch(PDOException $e) {
    print('Error : ' . $e->getMessage());
    exit();
}
    //формируем таблицу пользователей
    //формируем html код в переменной, потом просто печатаем
    $table = '<table border="1">'; $table .= '<thead> <tr class="tr1">';
    $table .= '<th> user </th>'; $table .= '<th> name </th>';
    $table .= '<th> email </th>'; $table .= '<th> year </th>';
    $table .= '<th> gender </th>'; $table .= '<th> limb </th>';
    $table .= '<th> bio </th>'; $table .= '<th> 0 </th>';
    $table .= '<th> 1 </th>'; $table .= '<th> 2 </th>';
    $table .= '</tr> </thead>';
?>
    <form class="exit" action="" method="POST">
        <input class="exitbutton" type="submit" name="exit" value="exit" />
    </form>
    </div>
    <h3>Список всех пользователей</h3>
        <?php for ($i=0;$i<count($idbd);$i++) { ?>
           <?php
            $table .= '<tr class="tr1">';
            try {
                //записываем результаты запросов в массив для последовательного вывода
                $stmt = $db->prepare("SELECT * FROM userbase WHERE id=:i");
                $result = $stmt->execute(array("i" => $idbd[$i]['id']));
                $data = current($stmt->fetchAll(PDO::FETCH_ASSOC));
                $stmt1 = $db->prepare("SELECT * FROM usersuperpower WHERE id=:i");
                $result = $stmt1->execute(array("i" => $idbd[$i]['id']));
                $sp = $stmt1->fetchAll(PDO::FETCH_ASSOC);
                $powers = [];
                $q = 0;
                for ($ii = 0; $ii < count($sp); $ii++) {
                    $powers[$q] = $sp[$ii]['power'];
                    $q++;
                }
            }
             catch(PDOException $e) {
                print('Error : ' . $e->getMessage());
                exit();
            }
                $table .= '<td>'. $idbd[$i]['login'] .'</td>';
                $table .= '<td>'. $data['name'] .'</td>';
                $table .= '<td>'. $data['email'] .'</td>';
                $table .= '<td>'. $data['year'] .'</td>';
                $table .= '<td>'. $data['sex'] .'</td>';
                $table .= '<td>'. $data['limb'] .'</td>';
                $table .= '<td>'. $data['bio'] .'</td>';
                $table .= '<td>'. in_array('0',$powers) .'</td>';
                $table .= '<td>'. in_array('1',$powers) .'</td>';
                $table .= '<td>'. in_array('2',$powers) .'</td>';
                $table .= '</tr>';
                ?>
        <?php }
    $table .= '</table>';
    print($table);
    ?>
    <h3>Управление данными</h3>
    <form action="" method="POST">
    <select class = "selectlist" name="list" required size = "<?php print(count($idbd)) ?>" >
            <?php       //создаем цикл, который формирует список пользователей для выбора
            for ($i=0;$i<count($idbd);$i++) { ?>
            <option value="<?php print $idbd[$i]['id']; ?>"><?php
                    print('id: ');
                    printf($idbd[$i]['id']);
                    print(' / ');
                    printf($idbd[$i]['login']); ?></option>
            <?php }?>
    </select>
    <div>
    <input class = "editbtn" type="submit" name="edit" value="edit" />
    <input class = "editbtn" type="submit" name="delete" value="delete" />
    </div>
</form>
    <h3>Статистика по сверхспособностям</h3>
<?php
    //еще одна таблица в переменной
    $table1 = '<table border="1">';
    $table1 .= '<thead> <tr class="tr1">';
    $table1 .= '<th> 0 (god) </th>';
    $table1 .= '<th> 1 (clip) </th>';
    $table1 .= '<th> 2 (fly) </th>';
    $table1 .= '</tr> </thead>';
    $table1 .= '<tr class="tr1">';
    for ($i=0;$i<3;$i++) {
        try {
            $stmt = $db->prepare("SELECT id FROM usersuperpower WHERE power=:i");
            $result = $stmt->execute(array("i" => $i));
            $datap = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e) {
            print('Error : ' . $e->getMessage());
            exit();
        }
        //получаем массив пользователей с определенной способностью и считаем количество элементов
        $table1 .= '<td>'. count($datap) .'</td>';
}
    $table1 .= '</tr>';
    $table1 .= '</table>';
    print($table1);
}
else {
    if( isset( $_POST['exit'] ) ) {
        //удаляем все данные с эмулирования сессии и выходим из администратора посылая 401
        session_start();
        //очищаем сессию, так как подменяем ее в обычной форме под админом
        $_SESSION['uid']=0;
        $_SESSION['login']=0;
        session_destroy();
        header('HTTP/1.1 401 Unanthorized');
        header('WWW-Authenticate: Basic realm="My site"');
        //нельзя перебросить на главную страницу сразу
        exit();
    }
    if( isset( $_POST['delete'] ) )
    {
        //удаляем все данные о пользователе с определенным id
        $user = 'u35653';
        $pass = '4017880';
        $db = new PDO('mysql:host=localhost;dbname=u35653', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
        try {
            $stmt1 = $db->prepare("DELETE FROM userpassword where id=:id");
            $stmt1 -> bindParam(':id', $_POST['list']);
            $stmt1 -> execute();
            $stmt2 = $db->prepare("DELETE FROM usersuperpower where id=:id");
            $stmt2 -> bindParam(':id', $_POST['list']);
            $stmt2 -> execute();
            $stmt3 = $db->prepare("DELETE FROM userbase where id=:id");
            $stmt3 -> bindParam(':id', $_POST['list']);
            $stmt3 -> execute();
        }
        catch(PDOException $e) {
            print('Error : ' . $e->getMessage());
            exit();
        }
        print_r("Запись удалена");
    }
    if ( isset( $_POST['edit'] ) ){
        //при редактировании подменяем сессию исходной формы на index.php и открываем ее
        $user = 'u35653';
        $pass = '4017880';
        $db = new PDO('mysql:host=localhost;dbname=u35653', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
        try {
            $stmt1 = $db->prepare("SELECT login FROM userpassword WHERE id=:i");
            $result = $stmt1->execute(array("i"=> $_POST['list']));
            $login = (current(current($stmt1->fetchAll(PDO::FETCH_ASSOC))));
        }
        catch(PDOException $e) {
            print('Error : ' . $e->getMessage());
            exit();
        }
        //эмулируем обычную сессию с уникальным uid
        session_start();
        $_SESSION['uid'] = substr( str_shuffle( 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM' ), 0, 10 );
        $_SESSION['login'] = $login;
        //бросаем на форму
        header('Location: ./');
        exit();
    }
} ?>
</body>
</html>
